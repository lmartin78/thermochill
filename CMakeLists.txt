cmake_minimum_required(VERSION 3.10)

# set the project name and version
project(ag_cooling VERSION 0.2)

# specify the C++ standard
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED True)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

set(MIDASSYS $ENV{MIDASSYS})
set(LIBS ${LIBS} -lpthread -lutil -lrt -ldl)

add_compile_options(-Wall -Wuninitialized -O2)
include_directories(include)

add_library(thermochill src/ThermoChill.cxx)

target_compile_options (thermochill PUBLIC -Wno-psabi)

link_libraries(thermochill)

add_executable(tchilltest progs/tchilltest.cc)

add_executable(feTChill progs/feTChill.cxx)

target_include_directories(feTChill PRIVATE ${MIDASSYS}/include ${MIDASSYS}/mxml ${MIDASSYS}/mjson ${MIDASSYS}/mvodb)
target_link_directories(feTChill PRIVATE ${MIDASSYS}/lib)
target_link_libraries(feTChill midas ${LIBS})

install(TARGETS feTChill tchilltest DESTINATION ${CMAKE_SOURCE_DIR}/bin)

# find_package(Doxygen
#              OPTIONAL_COMPONENTS dot)
# if(DOXYGEN_FOUND)
# 	set(DOXYGEN_OUTPUT_DIRECTORY docs)
#         doxygen_add_docs(
#                 doxygen
#                 ${PROJECT_SOURCE_DIR/include}
#                 ${PROJECT_SOURCE_DIR/progs}
#                 COMMENT "Generate documentation"
# 		)
# endif()
