#include "ThermoChill.hh"
#include <dirent.h>

ThermoChill::ThermoChill()
{
  fprintf(stderr, "ThermoChill::ctor!\n");
  memset(&tioTC,0,sizeof(tioTC));
  ttyTC = -1;
}

ThermoChill::~ThermoChill(){
  Disconnect();
}

void ThermoChill::Disconnect(){
  close(ttyTC);
  ttyTC = 0;
}

int ThermoChill::SetTermAttr(){
  if(ttyTC < 0){
    std::cerr << "ThermoChill: bad device" << std::endl;
    return 1;
  }
  if(tcgetattr(ttyTC, &tioTC) < 0){
    std::cerr << "ThermoChill: termios read error" << std::endl;
    return 2;
  }
  tioTC.c_iflag=0;
  tioTC.c_oflag=0;
  tioTC.c_cflag=CS8|CREAD|CLOCAL;           // 8n1, see termios.h for more informatioTCn
  tioTC.c_lflag=0;
  tioTC.c_cc[VMIN]=0;
  tioTC.c_cc[VTIME]=5;
  cfsetospeed(&tioTC,B9600);            // 9600 baud
  cfsetispeed(&tioTC,B9600);            // 9600 baud

  tcflush( ttyTC, TCIFLUSH );
  if(tcsetattr(ttyTC, TCSANOW, &tioTC) < 0){
    std::cerr << "ThermoChill: termios write error" << std::endl;
    return 3;
  }
  return 0;
}

int ThermoChill::Exchange(uint8_t cmd, int16_t &value){
  if(!ttyTC) return 0xee;
  char buf[16];
  buf[0] = 0xCA;
  buf[1] = 0;
  buf[2] = 1;
  buf[3] = cmd;

  int n = 0;
  if(cmd < 0xf0){
    buf[4] = 0;
    buf[5] = ~(buf[1]+buf[2]+buf[3]+buf[4]);
    n = 6;
  } else {
    buf[4] = 2;
    buf[5] = value >> 8;
    buf[6] = value;
    buf[7] = ~(buf[1]+buf[2]+buf[3]+buf[4]+buf[5]+buf[6]);
    n = 8;
  }
  tcflush(ttyTC,TCIOFLUSH);
  int nw = write(ttyTC,buf,n);
  if(nw != n){
      std::cerr << "Didn't write whole message! " << nw << " != " << n <<  std::endl;
  }
  usleep(200000);
  tcdrain(ttyTC);
  tcflush(ttyTC,TCOFLUSH);
  usleep(200000);
  int nr = read(ttyTC,&buf,16);
  if(verbose){
    std::cout << "Read " << nr << " bytes: ";
    for(int i = 0; i < nr; i++){
      std::cout << std::hex << std::setw(2) << std::setfill('0') << int(buf[i] & 0xff) << ' ';
    }
    std::cout <<std::endl;
  }
  uint8_t qb = 0xff;
  if(nr == 8){
    memcpy(&value, &buf[5], sizeof(value));
    // value = *(int16_t*)&buf[5];  // This gets a warning, the above should do the same
  } else if(nr > 8) {
    qb = buf[5];
    char *vc = (char*)&value;
    vc[0] = buf[7];
    vc[1] = buf[6];
  } else {
    std::cerr << "Not enough bytes received! " << nr <<  " < " << 8 << std::endl;
    return 0xee;
  }
  uint8_t checksum = 0;
  for(int i = 1; i < nr-1; i++){
    checksum += buf[i];
  }
  checksum = ~checksum;
  uint8_t cs = buf[nr-1];
  if(checksum != cs){
    std::cerr << "Reply checksum mismatch! " << std::hex << int(checksum) << " != " << int(cs) << std::endl;
    return 0xee;
  }
  return qb;
}

int ThermoChill::Connect(std::string devstr){
  std::vector<std::string> dev;
  if(devstr.size()) dev.push_back(devstr);
  else dev = FindTtyUSB();
  if(dev.size() < 1){
    std::cerr << "No ttyUSB devices found." << std::endl;
    return 1;
  }
  bool found(false);
  for(unsigned int i = 0; i < dev.size(); i++){
    if(!found){
      std::cout << "Checking device " << i << ": " << dev[i] << std::endl;
      ttyTC=open(dev[i].c_str(), O_RDWR);
      usleep(100000);
      if(ttyTC < 0){
	std::cerr << "Cannot open " << dev[i] << std::endl;
	continue;
      }
      if(SetTermAttr() > 0){
	std::cerr << "Cannot open term " << dev[i] << std::endl;
	continue;
      }
      if(CheckConnection()){
	std::cout << "ThermoChill found." << std::endl;
	sleep(1);
	found = true;
	connected = true;
	break;
      }
    }
  }
  if(found) return 0;
  else return 2;
}

bool ThermoChill::CheckConnection(){
  int16_t val;
  uint8_t qb = Exchange(0, val);
  // if(qb == 0xEE){
  //     usleep(100000);
  //     std::cerr << "ThermoChill retry" << std::endl;
  //     qb = Exchange(0, val);
  // }
  if(qb == 0xFF)
    return true;
  else {
    std::cerr << "ThermoChill responds with qb: " << int(qb) << std::endl;
    return false;
  }
}

std::vector<std::string> ThermoChill::FindTtyUSB(){
  DIR *dp;
  struct dirent *ep;
  std::vector<std::string> hits;
  dp = opendir ("/dev/");
  if (dp != NULL)
    {
      while ((ep = readdir (dp))){
	std::string f(ep->d_name);
	if(f.substr(0,6) == std::string("ttyUSB")){
	  f.insert(0,"/dev/");
	  hits.push_back(f);
	}
      }
      (void) closedir (dp);
    }
  else
    perror ("Couldn't open the directory");
  return hits;
}

bool ThermoChill::SetTemp(int16_t tset){
  int16_t t = tset;
  uint8_t qb = Exchange(0xF0, t);
  uint8_t prec = qb >> 4;
  assert(prec == 0);
  return(t == tset);
}

double ThermoChill::ReadTemp(){
  int16_t value = 0;
  uint8_t qb = Exchange(0x20, value);
  char unit = ' ';
  double temp = value;
  if(qb > 0x20){
    std::cerr << "Wrong QB: " << std::hex << std::setw(2) << std::setfill('0') << qb << std::endl;
  } else {
    uint8_t prec = qb >> 4;
    qb &= 0x0F;

    if(qb){
      unit = 'C';
    }
    switch(prec){
    case 1:
      temp /= 10.;
      break;
    case 2:
      temp /= 100.;
      break;
    }
    if(verbose) std::cout << "Temperature readback: " << temp << unit << std::endl;
  }
  return temp;
}

double ThermoChill::ReadSetPoint(){
  int16_t value = 0;
  uint8_t qb = Exchange(0x70, value);
  char unit = ' ';
  double temp = value;
  if(qb > 0x20){
    std::cerr << "Wrong QB: " << std::hex << std::setw(2) << std::setfill('0') << qb << std::endl;
  } else {
    uint8_t prec = qb >> 4;
    qb &= 0x0F;

    if(qb){
      unit = 'C';
    }
    switch(prec){
    case 1:
      temp /= 10.;
      break;
    case 2:
      temp /= 100.;
      break;
    }
    if(verbose) std::cout << "SetPoint readback: " << temp << unit << std::endl;
  }
  return temp;
}

int16_t ThermoChill::GetStatus(){
  int16_t value;
  uint8_t qb = Exchange(9, value);
  if(qb == 0xEE){
      // usleep(100000);
      // std::cerr << "ThermoChill retry" << std::endl;
      // qb = Exchange(9, value);
  }
  if(verbose) {
    if(value == 0x0100){
      std::cout << "Chiller running OK" << std::endl;
    } else {
      std::cout << "Chiller reports status: " << value << std::endl;
    }
  }
  return value;
}

int ThermoChill::CheckStatus(){
  int16_t status = GetStatus();
  d1 = status;
  uint8_t d2 = status >> 4;
  if(verbose){
    std::cout << "D1 = " << std::hex << std::setw(2) << std::setfill('0') << int(d1) << std::endl;
    std::cout << "D2 = " << std::hex << std::setw(2) << std::setfill('0') << int(d2) << std::endl;
  }
  if(d1 >> 2){
    std::cout << "Warnings present! D1:" << std::endl;
    for(int i = 7; i >=0; i--){
      std::cout << 'b' << i << '\t' << int(d1 & (1<<i)) << std::endl;
    }
  }
  if(d1 & 1){
    return 0;
  } else {
    if(d1 & 2){
      std::cout << "Fault! D2:" << std::endl;
      for(int i = 7; i >=0; i--){
	std::cout << 'b' << i << '\t' << int(d2 & (1<<i)) << std::endl;
      }
      if(d2)
	return d2;
      else
	return 0xF;
    } else {
      std::cerr << "Neither failed nor running?" << std::endl;
      return 0xF;
    }
  }
}

uint8_t ThermoChill::GetWarnings(){
  return d1 & (0xF << 2);
}
