/*******************************************************************\

  Name:         tmfe_example_everything.cxx
  Created by:   K.Olchanski

  Contents:     Example Front end to demonstrate all functions of TMFE class

\********************************************************************/

#undef NDEBUG // midas required assert() to be always enabled

#include <stdio.h>
#include <math.h> // M_PI

#include "midas.h"
#include "tmfe.h"
#include "ThermoChill.hh"
#include <iostream>

using std::cout;
using std::cerr;
using std::endl;

void tchill_callback(INT hDB, INT hkey, INT index, void *feptr);

/// Midas equipment for the cooling system
class TChillEq :
   public TMFeEquipment
{
public:
   TChillEq(const char* eqname, const char* eqfilename) // ctor
      : TMFeEquipment(eqname, eqfilename)
   {
      printf("TChillEq::ctor!\n");

      // configure the equipment here:
      printf("TChillEq::ctor!\n");

      //fEqConfReadConfigFromOdb = false;
      fEqConfEventID = 1;
      fEqConfPeriodMilliSec = 1000;
      fEqConfLogHistory = 1;
      fEqConfWriteEventsToOdb = true;
      fEqConfEnablePoll = true; // enable polled equipment
      //fEqConfPollSleepSec = 0; // to create a "100% CPU busy" polling loop, set poll sleep time to zero
   }

   ~TChillEq() // dtor
   {
      printf("TChillEq::dtor!\n");
   }

   void HandleUsage()
   {
      printf("TChillEq::HandleUsage!\n");
   }

   TMFeResult HandleInit(const std::vector<std::string>& args)
   {
      printf("TChillEq::HandleInit!\n");
      fMfe->RegisterTransitionStartAbort();
      fEqConfReadOnlyWhenRunning = false; // overwrite ODB Common RO_RUNNING to false
      fEqConfWriteEventsToOdb = true; // overwrite ODB Common RO_ODB to true

      fOdbEqVariables->RD("T_chiller", &t_chill, true);
      fOdbEqVariables->RD("Tset_chiller", &t_chill, true);
      fOdbEqVariables->RB("chiller_ok", &tc_ok, true);
      bool tc_connected = tc.connected;
      fOdbEqVariables->RB("chiller_connected", &tc_connected, true);
      fOdbEqVariables->RI("chiller_warnings", &tc_warnings, true);
      fOdbEqSettings->RI("T_chiller", &t_chill_set, true);
      fOdbEqSettings->RS("Device", &t_chill_dev, true);
      if(tc.Connect(t_chill_dev)){
         printf("ThermoChill connection failed.\n");
      }
      if(tc.connected) tc.SetTemp(t_chill_set);

      char tmpbuf[80];
      sprintf(tmpbuf, "/Equipment/%s/Settings/", fEqName.c_str());
      printf("Prog name = %s\n", fMfe->fProgramName.c_str());
      // sprintf(tmpbuf, "/Equipment/%s/Settings/", fMfe->fProgramName.c_str());
      HNDLE hkey;
      if(db_find_link(fMfe->fDB, 0, tmpbuf, &hkey) == DB_SUCCESS){
         KEY key;
         db_get_key(fMfe->fDB, hkey, &key);
         db_watch(fMfe->fDB, hkey, tchill_callback, (void*)this);
         printf("Hotlink on for key %s\n", key.name);
      } else {
         printf("Can't watch directory\n");
      }

      EqSetStatus("Started...", "white");
      //EqStartPollThread();
      return TMFeOk();
   }

   TMFeResult HandleRpc(const char* cmd, const char* args, std::string& response)
   {
      fMfe->Msg(MINFO, "HandleRpc", "RPC cmd [%s], args [%s]", cmd, args);

      // RPC handler
      return TMFeOk();
   }

   TMFeResult HandleBeginRun(int run_number)
   {
      fMfe->Msg(MINFO, "HandleBeginRun", "Begin run %d!", run_number);
      EqSetStatus("Running", "#00FF00");
      return TMFeOk();
   }

   TMFeResult HandleEndRun(int run_number)
   {
      fMfe->Msg(MINFO, "HandleEndRun", "End run %d!", run_number);
      EqSetStatus("Stopped", "#FFFFFF");
      return TMFeOk();
   }

   TMFeResult HandlePauseRun(int run_number)
   {
      fMfe->Msg(MINFO, "HandlePauseRun", "Pause run %d!", run_number);
      EqSetStatus("Paused", "#FFFF00");
      return TMFeOk();
   }

   TMFeResult HandleResumeRun(int run_number)
   {
      fMfe->Msg(MINFO, "HandleResumeRun", "Resume run %d!", run_number);
      EqSetStatus("Running", "#00FF00");
      return TMFeOk();
   }

   TMFeResult HandleStartAbortRun(int run_number)
   {
      fMfe->Msg(MINFO, "HandleStartAbortRun", "Begin run %d aborted!", run_number);
      EqSetStatus("Stopped", "#FFFFFF");
      return TMFeOk();
   }

   void SendData(double dvalue)
   {
      char buf[1024];
      ComposeEvent(buf, sizeof(buf));
      BkInit(buf, sizeof(buf));

      double* ptr = (double*)BkOpen(buf, "data", TID_DOUBLE);
      *ptr++ = dvalue;
      BkClose(buf, ptr);

      EqSendEvent(buf);
   }

   void HandlePeriodic()
   {
      printf("TChillEq::HandlePeriodic!\n");
      // double t = TMFE::GetTime();
      static int errcount = 0;

      bool tc_connected = tc.CheckConnection();
      if(!tc_connected){
         tc.Disconnect();
         tc_connected = (tc.Connect(t_chill_dev)==0);
      }
      if(tc_connected){
         int status = tc.CheckStatus();
         if(status) cerr << "Chiller status: " << status << endl;
         tc_ok = (status == 0);
         errcount = 0;
      } else {
         tc_ok = false;
         errcount++;
         if(true || errcount == 1)
            fMfe->Msg(MERROR, "HandlePeriodic", "Chiller connection error %d!", errcount);
         if(errcount > 3){
            fMfe->Msg(MERROR, "HandlePeriodic", "Chiller connection broken!");
            exit(1);
         }
      }
      fOdbEqVariables->WB("chiller_connected", tc_connected);
      fOdbEqVariables->WB("chiller_ok", tc_ok);
      double t_set_rb = 0.;
      if(tc_connected){
         if(!tc_ok){
            static bool first = true;
            if(first)
               fMfe->Msg(MERROR, "HandlePeriodic", "Chiller error!");
            first = false;
         }
         tc_warnings = tc.GetWarnings();
         fOdbEqVariables->WI("chiller_warnings", tc_warnings);
         if(tc_warnings){
            fMfe->Msg(MERROR, "HandlePeriodic", "Chiller warnings! %x", tc_warnings);
         }
         t_set_rb = tc.ReadSetPoint();
         t_chill = tc.ReadTemp();
         fOdbEqVariables->WD("Tset_chiller", t_set_rb);
         fOdbEqVariables->WD("T_chiller", t_chill);
      }
      EqWriteStatistics();
      char status_buf[256];
      sprintf(status_buf, "Chiller running: Setpoint %.1fC, Temp %.1fC", t_set_rb, t_chill);
      EqSetStatus(status_buf, "#00FF00");
   }

   bool HandlePoll()
   {
      //printf("TChillEq::HandlePoll!\n");

      if (!fMfe->fStateRunning) // only poll when running
         return false;

      double r = drand48();
      if (r > 0.999) {
         // return successful poll rarely
         printf("TChillEq::HandlePoll!\n");
         return true;
      }

      return false;
   }

   void HandlePollRead()
   {
      printf("TChillEq::HandlePollRead!\n");

         char buf[1024];

         ComposeEvent(buf, sizeof(buf));
         BkInit(buf, sizeof(buf));

         uint32_t* ptr = (uint32_t*)BkOpen(buf, "poll", TID_UINT32);
         for (int i=0; i<16; i++) {
            *ptr++ = lrand48();
         }
         BkClose(buf, ptr);

         EqSendEvent(buf, false); // do not write polled data to ODB and history
   }
   void fecallback(HNDLE hkey, INT index){
      KEY key;
      int status = db_get_key(fMfe->fDB, hkey, &key);
      if(status == DB_SUCCESS){
         printf("ODB entry %s changed!\n", key.name);
      } else {
         printf("ODB error!\n");
         return;
      }
      std::string name(key.name);
      if(name == std::string("T_chiller")){
         fOdbEqSettings->RI(name.c_str(), &t_chill_set);
         if(tc.connected) tc.SetTemp(t_chill_set);
      } else {
         fMfe->Msg(MINFO, "fecallback", "Not sure what to do with ODB entry %s", name.c_str());
      }
   }
private:
   ThermoChill tc;
   std::string t_chill_dev;
   int t_chill_set = 16;
   double t_chill = 0.;
   int tc_warnings = 0;
   bool tc_ok = false;
};

// example frontend

class FeTChill: public TMFrontend
{
public:
   FeTChill() // ctor
   {
      printf("FeTChill::ctor!\n");
      FeSetName("feTChill");
      FeAddEquipment(new TChillEq("ThermoChill", __FILE__));
   }

   void HandleUsage()
   {
      printf("FeTChill::HandleUsage!\n");
   };

   TMFeResult HandleArguments(const std::vector<std::string>& args)
   {
      printf("FeTChill::HandleArguments!\n");
      return TMFeOk();
   };

   TMFeResult HandleFrontendInit(const std::vector<std::string>& args)
   {
      printf("FeTChill::HandleFrontendInit!\n");
      return TMFeOk();
   };

   TMFeResult HandleFrontendReady(const std::vector<std::string>& args)
   {
      printf("FeTChill::HandleFrontendReady!\n");
      //FeStartPeriodicThread();
      //fMfe->StartRpcThread();
      return TMFeOk();
   };

   void HandleFrontendExit()
   {
      printf("FeTChill::HandleFrontendExit!\n");
   };
};

void tchill_callback(INT hDB, INT hkey, INT index, void *feptr)
{
   TChillEq* fe = (TChillEq*)feptr;
   fe->fecallback(hkey, index);
}

// boilerplate main function

int main(int argc, char* argv[])
{
   FeTChill fe_everything;
   return fe_everything.FeMain(argc, argv);
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
