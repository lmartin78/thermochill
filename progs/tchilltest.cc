#include <iostream>
#include "ThermoChill.hh"

using std::cout;
using std::cerr;
using std::endl;

int main(){
  ThermoChill tc;
  if(tc.Connect("/dev/ttyUSB1")) return 1;
  if(tc.CheckStatus()==0){
    cout << "Connected OK!" << endl;
  } else {
    cerr << "Error!" << endl;
    return 1;
  }

  double t = tc.ReadTemp();
  std::cout << "Temperature: " << t << std::endl;

  if(tc.SetTemp(17)){
    std::cout << "Successfully set SetPoint" << std::endl;
  } else {
    std::cerr << "Trouble changing SetPoint" << std::endl;
  }

  while(true){
      sleep(5);
      int status = tc.CheckStatus();
      t = tc.ReadTemp();
      std::cout << "Status: " << status << ", Temperature: " << t << std::endl;
  }
  return 0;
}
