#include "CoolControl.hh"
#include <iostream>

int main(){
  CoolControl cooling;
  cooling.SetNAvg(10);
  cooling.SetWaterPumpSpeed(0);
  char c;
  bool doPid = false;
  while(true){
    c = getchar();
    switch(c){
    case 'v': cooling.ToggleVac(); break;
    case 'V': cooling.ToggleVent(); break;
    case 'w': cooling.ToggleWater(); break;
    case '+': cooling.IncPump(5); break;
    case '-': cooling.IncPump(-5); break;
    case 's': {
        int s;
        std::cout << "new pump speed?:" << std::endl;
        std::cin >> s;
        cooling.SetWaterPumpSpeed(s);
        break;
    }
    case 'p': {
        double p;
        std::cout << "new p coefficient?:" << std::endl;
        std::cin >> p;
        cooling.SetP(p);
        break;
    }
    case 'P': {
      cooling.PrimePump();
        break;
    }
    case 'i': {
        double i;
        std::cout << "new i coefficient?:" << std::endl;
        std::cin >> i;
        cooling.SetI(i);
        break;
    }
    case 'd': {
        double d;
        std::cout << "new d coefficient?:" << std::endl;
        std::cin >> d;
        cooling.SetD(d);
        break;
    }
    case 'l': {
        double bl;
        std::cout << "new backlight?:" << std::endl;
        std::cin >> bl;
        cooling.SetBacklight(bl);
        break;
    }
    case 'n': {
        int navg;
        std::cout << "N_avg?:" << std::endl;
        std::cin >> navg;
        cooling.SetNAvg(navg);
        break;
    }
    case 't': {
        double tavg;
        std::cout << "t_avg?:" << std::endl;
        std::cin >> tavg;
        cooling.SetPidTAvg(tavg);
        break;
    }
    case 'c': doPid = !doPid; cooling.EngageControl(doPid); break;
    case 'q': return 0;
    }
  }
  return 0;
}
